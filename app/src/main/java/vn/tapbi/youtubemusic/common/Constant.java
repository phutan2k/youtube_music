package vn.tapbi.youtubemusic.common;

public class Constant {
    public static  final String DATABASE_NAME ="tapbi_sample.realm";
    public static  final String DB_NAME ="TapbiSampleDb.db";
    public static  final int DB_VERSION =1;
    public static  final String BASE_URL ="https://youtube.googleapis.com/youtube/v3/";

    public static final String API_KEY = "AIzaSyC07U2YVNhL_yzyfQB2To0g6i23Os2rz9Q";

    public static  final int CONNECT_S =30;
    public static  final int READ_S =30;
    public static  final int WRITE_S =30;

    public static  final String  LANGUAGE_EN ="en";
    public static  final String  LANGUAGE_VN ="vi";

    /*Param fragment*/
    public static final String ARGUMENT_FRAGMENT_MESSAGE_ID="ARGUMENT_FRAGMENT_MESSAGE_ID";
    public static final String ARGUMENT_FRAGMENT_MESSAGE="ARGUMENT_FRAGMENT_MESSAGE";
    public static final String ARGUMENT_FRAGMENT_MESSAGE_TITLE="ARGUMENT_FRAGMENT_MESSAGE_TITLE";
    /*SharePreference constant*/
    public static final String USER_NAME="USER_NAME";
    public static final String LOAD_ALL_SMS_FIRST_TIME="LOAD_ALL_SMS_FIRST_TIME";
    public static final String PREF_SETTING_LANGUAGE="pref_setting_language";

    /*switch type db, this only use in this sample*/
    public static  final int  DB_TYPE_ROOM =1;
    public static  final int  DB_TYPE_REALM =2;
    public static  final int  DB_TYPE =DB_TYPE_ROOM;

    /*event constant*/
    public static  final int  EVENT_BACK_PREVIOUS_SCREEN =1;
    public static  final int  EVENT_SHOW_TOAST =2;
    public static  final int  EVENT_CHANGE_CONFIG =3;

    /*Query Param*/
    public static final int MAX_RESULT = 100;
    public static final String REGION_CODE = "VN";
    public static final String VIDEO = "VIDEO";

    /*Event Bus*/
    public static final int PLAY_VIDEO = 5;
    public static final int PAUSE_VIDEO = 6;

    /*Setting pager fragment*/
    public static final int TYPE_HOME = 0;
    public static final String ON_PLAY_BACKGROUND = "ON_PLAY_BACKGROUND";
    public static final String OFF_PLAY_BACKGROUND = "OFF_PLAY_BACKGROUND";
    public static final String ON_NOTIFICATION = "ON_NOTIFICATION";
    public static final String OFF_NOTIFICATION = "OFF_NOTIFICATION";
    public static final int TYPE_TRENDING = 7;
    public static final String LINK_GOOGLE = "https://support.google.com/youtube/answer/9288567?hl=vi";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String Alarm_ID = "Alarm_ID";
    public static final String PRIORITY_CHANNEL_ID = "PRIORITY_CHANNEL_ID";
    public static final String PRIORITY_Alarm_ID = "PRIORITY_Alarm_ID";
    public static final int REQUEST_CODE_1 = 1;
    public static final int NOTIFICATION_ID_1 = 1;

    /*Top fragment*/
    public static final int NEXT_VIDEO = 3;
    public static final int ENTER_FULL_SCREEN = 50;
    public static final int EXIT_FULL_SCREEN = 51;
    public static final int SHOW_NAVIGATION = 52;
    public static final int HIDE_NAVIGATION = 53;
    public static final int PREVIOUS_VIDEO = 54;
    public static final int TYPE_VIDEO = 55;
    public static final int TYPE_CHANNEL = 56;
    public static final int TYPE_IMG = 57;
    public static final int LEVEL_PLAY = 58;
    public static final int LEVEL_STOP = 59;
    public static final int LOADING = 60;
    public static final int SEEK_BAR = 61;
    public static final int TYPE_TITLE = 62;
    public static final int TYPE_TITLE_CHANNEL = 63;
    public static final int HIDE_NAVIGATION_TO_WEB_VIEW = 64;
}
