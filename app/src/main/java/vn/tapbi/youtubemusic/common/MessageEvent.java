package vn.tapbi.youtubemusic.common;

import java.util.List;

import vn.tapbi.youtubemusic.data.model.video.Item;

public class MessageEvent {
    private int typeEvent = 0;
    private String stringValue = "";
    private Item item;
    private List<Item> listItem;
    private boolean isBoolean;

    public MessageEvent(Item item) {
        this.item = item;
    }

    public MessageEvent(int typeEvent, String stringValue) {
        this.typeEvent = typeEvent;
        this.stringValue = stringValue;
    }

    public MessageEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public MessageEvent(List<Item> listItem) {
        this.listItem = listItem;
    }

    public MessageEvent(int typeEvent, boolean isBoolean) {
        this.typeEvent = typeEvent;
        this.isBoolean = isBoolean;
    }

    public int getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Item getItem() {
        return item;
    }

    public List<Item> getListItem() {
        return listItem;
    }

    public boolean isBoolean() {
        return isBoolean;
    }

    public void setBoolean(boolean aBoolean) {
        isBoolean = aBoolean;
    }
}