package vn.tapbi.youtubemusic.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;


import timber.log.Timber;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.ui.main.MainActivity;

public class AlarmReceiver extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, MainActivity.class);
        NotificationChannel notificationChannel = new NotificationChannel(Constant.Alarm_ID, Constant.PRIORITY_Alarm_ID, NotificationManager.IMPORTANCE_LOW);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 2, intent, 0);
        Notification notification = new Notification.Builder(context, Constant.Alarm_ID)
                .setContentText(context.getString(R.string.text_notify))
                .setContentTitle(context.getString(R.string.title_notify))
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_shape_icon)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(2, notification);
        Timber.e("techJA");
    }
}

