package vn.tapbi.youtubemusic.ui.main.web_view;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.RequiresApi;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.rxjava3.annotations.NonNull;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.common.MessageEvent;
import vn.tapbi.youtubemusic.databinding.FragmentWebviewBinding;
import vn.tapbi.youtubemusic.ui.base.BaseBindingFragment;
import vn.tapbi.youtubemusic.ui.custom.MyWebViewClient;

public class WebViewFragment extends BaseBindingFragment<FragmentWebviewBinding, WebViewViewModel> {
    private static final String TAG = WebViewFragment.class.getName();

    @Override
    protected Class<WebViewViewModel> getViewModel() {
        return WebViewViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_webview;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        binding.webView.loadUrl("https://www.google.com.vn/");
        binding.webView.setWebViewClient(new MyWebViewClient());

        binding.ivBack.setOnClickListener(v -> {

        });

        EventBus.getDefault().post(new MessageEvent(Constant.HIDE_NAVIGATION_TO_WEB_VIEW));
    }

    @Override
    protected void onPermissionGranted() {

    }
}
