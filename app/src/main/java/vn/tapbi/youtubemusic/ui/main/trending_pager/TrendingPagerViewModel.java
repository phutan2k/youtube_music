package vn.tapbi.youtubemusic.ui.main.trending_pager;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.data.model.channel.Channel;
import vn.tapbi.youtubemusic.data.model.video.Video;
import vn.tapbi.youtubemusic.data.respository.YoutubeRepository;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class TrendingPagerViewModel extends BaseViewModel {
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<Video> videoMutableLiveData = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public TrendingPagerViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getVideo(){
        youtubeRepository.getVideo(Constant.MAX_RESULT, Constant.API_KEY, Constant.REGION_CODE)
                .subscribe(new SingleObserver<Video>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Video video) {
                        videoMutableLiveData.postValue(video);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.txt_api_key_died);
                    }
                });
    }
}
