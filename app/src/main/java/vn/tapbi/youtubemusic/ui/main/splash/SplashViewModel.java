package vn.tapbi.youtubemusic.ui.main.splash;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class SplashViewModel extends BaseViewModel {
    @Inject
    public SplashViewModel() {
    }
}
