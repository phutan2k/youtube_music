package vn.tapbi.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.data.local.entities.Suggestion;
import vn.tapbi.youtubemusic.databinding.ItemRecentSearchBinding;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.RecentSearchHolder> {
    private final Context mContext;
    private List<Suggestion> listSuggestion;
    private final MutableLiveData<String> itemResult = new MutableLiveData<>();

    public RecentSearchAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListSuggestion(List<Suggestion> listSuggestion) {
        this.listSuggestion = listSuggestion;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecentSearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRecentSearchBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_recent_search, parent, false);
        return new RecentSearchHolder(viewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentSearchHolder holder, int position) {
        holder.bind(listSuggestion.get(position));
    }

    @Override
    public int getItemCount() {
        if (listSuggestion != null) {
            return listSuggestion.size();
        }
        return 0;
    }

    public class RecentSearchHolder extends RecyclerView.ViewHolder {
        ItemRecentSearchBinding viewBinding;

        public RecentSearchHolder(@NonNull ItemRecentSearchBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        public void bind(Suggestion item) {
            viewBinding.tvTitle.setTag(item.name);
            viewBinding.tvTitle.setText(item.name);

            viewBinding.clMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDetailVideo(viewBinding.tvTitle.getTag());
                }
            });
        }
    }

    private void showDetailVideo(Object tag) {
        itemResult.postValue((String) tag);
    }

    public MutableLiveData<String> getItemResult() {
        return itemResult;
    }
}
