package vn.tapbi.youtubemusic.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.data.local.SharedPreferenceHelper;
import vn.tapbi.youtubemusic.databinding.ViewPlayBackgroundBinding;
import vn.tapbi.youtubemusic.ui.base.BaseBindingDialogFragment;

public class PlayBackgroundDialog extends BaseBindingDialogFragment<ViewPlayBackgroundBinding> {
    private int type;
    private SharedPreferenceHelper sharedPreferenceHelper;

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_play_background;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.9),
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        sharedPreferenceHelper = new SharedPreferenceHelper(requireContext(),
                Constant.PREF_SETTING_LANGUAGE);
        if (type == Constant.TYPE_HOME) {
            initView(requireContext().getString(R.string.txt_play_background)
                    , sharedPreferenceHelper.getBoolean(Constant.ON_PLAY_BACKGROUND, false), Constant.ON_PLAY_BACKGROUND, Constant.OFF_PLAY_BACKGROUND);
        } else if (type == Constant.TYPE_TRENDING) {
            initView(requireContext().getString(R.string.txt_get_notications_in_morning), sharedPreferenceHelper.getBoolean(Constant.ON_NOTIFICATION, false),
                    Constant.ON_NOTIFICATION, Constant.OFF_NOTIFICATION);
        }
    }

    private void initView(String text, boolean check, String idOn, String idOff) {
        binding.txtTitle.setText(text);
        if (check)
            binding.rbOn.setChecked(true);
        else {
            binding.rbOff.setChecked(true);
        }
        binding.rbOn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sharedPreferenceHelper.storeBoolean(idOn, isChecked);
        });
        binding.rbOff.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sharedPreferenceHelper.storeBoolean(idOff, isChecked);
        });
        binding.btButton.setOnClickListener(v -> dismiss());
    }
}
