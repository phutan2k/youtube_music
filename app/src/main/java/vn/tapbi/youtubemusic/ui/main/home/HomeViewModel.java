package vn.tapbi.youtubemusic.ui.main.home;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class HomeViewModel extends BaseViewModel {
    @Inject
    public HomeViewModel() {
    }
}
