package vn.tapbi.youtubemusic.ui.main.search_pager;

import androidx.lifecycle.MutableLiveData;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.data.local.entities.Suggestion;
import vn.tapbi.youtubemusic.data.model.search.Search;
import vn.tapbi.youtubemusic.data.respository.SuggestionRepository;
import vn.tapbi.youtubemusic.data.respository.YoutubeRepository;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class SearchPagerViewModel extends BaseViewModel {
    public final MutableLiveData<Search> newSearch = new MutableLiveData<>();
    public final MutableLiveData<Search> searchVideo = new MutableLiveData<>();
    public final MutableLiveData<List<Suggestion>> suggestionMutableLiveData = new MutableLiveData<>();
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public SearchPagerViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getVideoSearch(String title) {
        youtubeRepository.getHandlerVideoSearch(Constant.MAX_RESULT, title, Constant.VIDEO, Constant.API_KEY)
                .subscribe(new SingleObserver<Search>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Search search) {
                        newSearch.postValue(search);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.txt_api_key_died);
                    }
                });
    }

    public void getSearchVideo(String title) {
        youtubeRepository.getHandlerVideoSearch(Constant.MAX_RESULT, title, Constant.VIDEO, Constant.API_KEY)
                .subscribe(new SingleObserver<Search>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Search search) {
                        searchVideo.postValue(search);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.txt_api_key_died);
                    }
                });
    }

    public void addSuggestion(Suggestion suggestion){
        SuggestionRepository.getInstance().addSuggestion(suggestion);
    }

//    public void getSuggestion() {
//        SuggestionRepository.getInstance().getListSuggestion().subscribe(
//                new SingleObserver<List<Suggestion>>() {
//                    @Override
//                    public void onSubscribe(@NonNull Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(@NonNull List<Suggestion> suggestions) {
//                        Collections.reverse(suggestions);
//                        suggestionMutableLiveData.postValue(suggestions);
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//                        e.printStackTrace();
//                    }
//                });
//    }

}
