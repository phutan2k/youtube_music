package vn.tapbi.youtubemusic.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import dagger.hilt.android.AndroidEntryPoint;
import vn.tapbi.youtubemusic.utils.LocaleUtils;

@AndroidEntryPoint
public abstract class BaseFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        LocaleUtils.applyLocale(requireContext());
        super.onCreate(savedInstanceState);
    }
}
