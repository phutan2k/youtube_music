package vn.tapbi.youtubemusic.ui.main.web_view;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class WebViewViewModel extends BaseViewModel {
    @Inject
    public WebViewViewModel() {
    }
}
