package vn.tapbi.youtubemusic.ui.main.drag_top;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class TopViewModel extends BaseViewModel {
    @Inject
    public TopViewModel() {
    }
}
