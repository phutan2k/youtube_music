package vn.tapbi.youtubemusic.ui.main;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.RequiresApi;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.Date;

import timber.log.Timber;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.common.MessageEvent;
import vn.tapbi.youtubemusic.data.local.SharedPreferenceHelper;
import vn.tapbi.youtubemusic.databinding.ActivityMainBinding;
import vn.tapbi.youtubemusic.receiver.AlarmReceiver;
import vn.tapbi.youtubemusic.ui.base.BaseBindingActivity;
import vn.tapbi.youtubemusic.utils.Utils;

public class MainActivity extends BaseBindingActivity<ActivityMainBinding, MainViewModel> {
    public NavController navControllerMain;
    public NavHostFragment navHostFragmentMain;
    private NavGraph graph;

    private NotificationManager notificationManager;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private String channelVideo;
    private String titleVideo;
    private String imageVideo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void setupView(Bundle savedInstanceState) {
        Timber.e("tanLP .");
        navHostFragmentMain = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainerView);
        if (navHostFragmentMain != null) {
            navControllerMain = navHostFragmentMain.getNavController();
        }
        Handler handler = new Handler(this.getMainLooper());
        handler.postDelayed(() -> changeMainScreen(R.id.homeFragment, null), 2000);

    }

    public void changeMainScreen(int idScreen, Bundle bundle) {
        if (graph == null) {
            graph = navControllerMain.getNavInflater().inflate(R.navigation.nav_main);
        }
        graph.setStartDestination(idScreen);
        navControllerMain.setGraph(graph, bundle);
    }

    @Override
    public void setupData() {

    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void showNotification() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        NotificationChannel notificationChannel = new NotificationChannel(Constant.CHANNEL_ID, Constant.PRIORITY_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), Constant.REQUEST_CODE_1, intent, 0);
        Notification notification = new Notification.Builder(getApplicationContext(), Constant.CHANNEL_ID).setLargeIcon(Utils.getIcon(imageVideo))
                .setContentText(channelVideo)
                .setContentTitle(titleVideo)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_shape_icon)
                .build();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(Constant.NOTIFICATION_ID_1, notification);
    }

    public void closeNotification() {
        if (notificationManager != null) {
            notificationManager.cancel(Constant.NOTIFICATION_ID_1);
        }
    }

    public void StartAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 7);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        if (calendar.getTime().compareTo(new Date()) < 0)
            calendar.add(Calendar.DAY_OF_MONTH, 1);

        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Timber.e("TechJA Android");
    }

    public void showGetNotificationMorning() {
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getApplicationContext(), Constant.PREF_SETTING_LANGUAGE);
        if (sharedPreferenceHelper.getBoolean(Constant.ON_NOTIFICATION, false)) {
            StartAlarm();
        } else if (sharedPreferenceHelper.getBoolean(Constant.OFF_NOTIFICATION, false)) {
            if (alarmManager != null) {
                alarmManager.cancel(pendingIntent);
                Timber.e("TechJA off");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showGetNotificationMorning();
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 2) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMyDesign(MessageEvent event) {
        switch (event.getTypeEvent()) {
            case Constant.TYPE_VIDEO:
                titleVideo = event.getStringValue();
                break;
            case Constant.TYPE_CHANNEL:
                channelVideo = event.getStringValue();
                break;
            case Constant.TYPE_IMG:
                imageVideo = event.getStringValue();
                break;
        }
    }
}