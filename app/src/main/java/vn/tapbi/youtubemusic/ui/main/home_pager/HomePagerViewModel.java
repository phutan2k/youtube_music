package vn.tapbi.youtubemusic.ui.main.home_pager;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.data.model.search.Search;
import vn.tapbi.youtubemusic.data.respository.YoutubeRepository;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class HomePagerViewModel extends BaseViewModel {
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<Search> itemVideoSearch = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public HomePagerViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getVideoSearch() {
        youtubeRepository.fixSearchApi(Constant.MAX_RESULT, "ncs",
                Constant.VIDEO, Constant.API_KEY).subscribe(new SingleObserver<Search>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull Search search) {
                itemVideoSearch.postValue(search);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.txt_api_key_died);
            }
        });
    }
}

