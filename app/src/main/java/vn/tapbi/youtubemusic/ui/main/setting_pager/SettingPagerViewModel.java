package vn.tapbi.youtubemusic.ui.main.setting_pager;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class SettingPagerViewModel extends BaseViewModel {
    @Inject
    public SettingPagerViewModel() {
    }
}
