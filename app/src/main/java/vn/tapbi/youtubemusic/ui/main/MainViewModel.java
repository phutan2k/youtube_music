package vn.tapbi.youtubemusic.ui.main;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import vn.tapbi.youtubemusic.ui.base.BaseViewModel;

@HiltViewModel
public class MainViewModel extends BaseViewModel {
    public  MutableLiveData<String> stringMutableLiveData=new MutableLiveData<>();
    @Inject
    public MainViewModel() {
    }
}
