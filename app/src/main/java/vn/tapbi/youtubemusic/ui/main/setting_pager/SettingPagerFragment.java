package vn.tapbi.youtubemusic.ui.main.setting_pager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import vn.tapbi.youtubemusic.R;
import vn.tapbi.youtubemusic.common.Constant;
import vn.tapbi.youtubemusic.databinding.SettingPagerFragmentBinding;
import vn.tapbi.youtubemusic.ui.base.BaseBindingFragment;
import vn.tapbi.youtubemusic.ui.dialog.LanguageDialog;
import vn.tapbi.youtubemusic.ui.dialog.PlayBackgroundDialog;

public class SettingPagerFragment extends BaseBindingFragment<SettingPagerFragmentBinding,
        SettingPagerViewModel> implements View.OnClickListener {
    private static final String TAG = SettingPagerFragment.class.getName();

    @Override
    protected Class<SettingPagerViewModel> getViewModel() {
        return SettingPagerViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.setting_pager_fragment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        binding.tvLanguage.setOnClickListener(this);
        binding.tvBackground.setOnClickListener(this);
        binding.tvNotify.setOnClickListener(this);
        binding.tvPolicy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_language) {
//            v.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.abc_fade_in));
            getLanguage();
        } else if (v.getId() == R.id.tv_background) {
//            v.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.abc_fade_in));
            getPlayBackground();
        } else if (v.getId() == R.id.tv_notify) {
//            v.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.abc_fade_in));
            getNotify();
        } else if (v.getId() == R.id.tv_policy) {
//            v.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.abc_fade_in));
            getPolicy();
        }
    }

    private void getPolicy() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.LINK_GOOGLE));
        startActivity(browserIntent);

//        FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.ln_home, new WebViewFragment(), TAG);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
    }

    private void getNotify() {
        showDialogConfirm(Constant.TYPE_TRENDING);
    }

    private void showDialogConfirm(int type) {
        PlayBackgroundDialog dialogFragment = new PlayBackgroundDialog();
        dialogFragment.setType(type);
        dialogFragment.show(requireActivity().getSupportFragmentManager(), null);
    }

    private void getPlayBackground() {
        showDialogConfirm(Constant.TYPE_HOME);
    }

    private void getLanguage() {
        LanguageDialog dialog = new LanguageDialog();
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }

    @Override
    protected void onPermissionGranted() {

    }
}
