package vn.tapbi.youtubemusic;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.room.Room;

import dagger.hilt.android.HiltAndroidApp;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import timber.log.Timber;
import vn.tapbi.youtubemusic.data.local.SuggestionDB;
import vn.tapbi.youtubemusic.utils.LocaleUtils;
import vn.tapbi.youtubemusic.utils.MyDebugTree;

@HiltAndroidApp
public class App extends MultiDexApplication {
    private static App instance;
    private SuggestionDB suggestionDB;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        RxJavaPlugins.setErrorHandler(Timber::w);
        initLog();
        initConfig();
    }

    private void initConfig() {
        suggestionDB = Room.databaseBuilder(this,
                SuggestionDB.class, "suggestion-db").build();
    }

    public SuggestionDB getSuggestionDB() {
        return suggestionDB;
    }

    private void initLog() {
        if (BuildConfig.DEBUG) {
            Timber.e("tunglt onCreate: ");

            Timber.plant(new MyDebugTree());
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.applyLocale(this);
    }
}
