package vn.tapbi.youtubemusic.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import vn.tapbi.youtubemusic.data.local.entities.Suggestion;

@Dao
public interface SuggestionDAO {
//    @Query("SELECT * FROM suggestion")
//    Single<List<Suggestion>> getSuggestions();

    @Query("SELECT * FROM suggestion")
    List<Suggestion> getSuggestions();

    @Insert
    void insertSuggestion(Suggestion... suggestions);

    @Delete
    void deleteSuggestion(Suggestion... suggestions);
}
