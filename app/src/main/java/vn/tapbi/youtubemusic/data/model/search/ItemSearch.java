package vn.tapbi.youtubemusic.data.model.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import vn.tapbi.youtubemusic.data.model.channel.Channel;
import vn.tapbi.youtubemusic.data.model.item_video.ItemVideo;
import vn.tapbi.youtubemusic.data.model.video.Item;
import vn.tapbi.youtubemusic.data.model.video.VideoItem;

public class ItemSearch {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("id")
    @Expose
    private IdSearch id;
    @SerializedName("snippet")
    @Expose
    private SnippetSearch snippet;

    private Channel channel;

    private Item item;

    private ItemVideo itemVideo;

    private VideoItem videoItem;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public IdSearch getId() {
        return id;
    }

    public void setId(IdSearch id) {
        this.id = id;
    }

    public SnippetSearch getSnippet() {
        return snippet;
    }

    public void setSnippet(SnippetSearch snippet) {
        this.snippet = snippet;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public ItemVideo getItemVideo() {
        return itemVideo;
    }

    public void setItemVideo(ItemVideo itemVideo) {
        this.itemVideo = itemVideo;
    }

    public VideoItem getVideoItem() {
        return videoItem;
    }

    public void setVideoItem(VideoItem videoItem) {
        this.videoItem = videoItem;
    }
}
