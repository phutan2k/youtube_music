package vn.tapbi.youtubemusic.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import vn.tapbi.youtubemusic.data.local.dao.SuggestionDAO;
import vn.tapbi.youtubemusic.data.local.entities.Suggestion;

@Database(version = 1, entities = {Suggestion.class})
public abstract class SuggestionDB extends RoomDatabase {
    public abstract SuggestionDAO getSuggestionDAO();
}
